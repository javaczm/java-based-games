package com.gobang;

import java.util.HashMap;

/**
 * 用权值实现的AI算法
 * 根据当前位置的 前后 左右 左上斜右下斜 右上斜左下斜 的棋子分布情况，定位权重值
 */
public class EasyAI implements Gobang {
    static HashMap<String,Integer> map = new HashMap<>();
    static {
        /**
         * 权值
         */
        // 防守权值
        // 活1连
        map.put("010", 20);   // 1颗棋子
        map.put("0-10", 15);
        // 眠1连
        map.put("-110", 1);
        map.put("1-10", 1);
        // 活2连
        map.put("0110", 200);  // 2颗棋子
        map.put("0-1-10", 150);
        // 眠2连
        map.put("-1110", 20);
        map.put("1-1-10", 10);
        // 活3连
        map.put("01110", 7000);  // 3颗棋子
        map.put("0-1-1-10", 5250);
        // 眠3连
        map.put("-11110", 50);
        map.put("1-1-1-10", 30);
        // 活4连
        map.put("011110", 10000);  // 4颗棋子
        map.put("0-1-1-1-10", 10000);
        // 眠4连
        map.put("-111110", 10000);
        map.put("1-1-1-1-10", 10000);
        // 碰壁眠4连
        map.put("11110", 10000);  //死角
        map.put("-1-1-1-10", 10000);

        // //进攻权值
        // //活1连
        // map.put("010",10);
        // map.put("0-10",20);
        // //眠1连
        // map.put("-110",1);
        // map.put("1-10",1);
        // //活2连
        // map.put("0110", 100);
        // map.put("0-1-10",200);
        // //眠2连
        // map.put("-1110",10);
        // map.put("1-1-10",20);
        // //活3连
        // map.put("01110", 5000);
        // map.put("0-1-1-10",7000);
        // //眠3连
        // map.put("-11110",30);
        // map.put("1-1-1-10",50);
        // //活4连
        // map.put("011110", 10000);
        // map.put("0-1-1-1-10",10000);
        // //眠4连
        // map.put("-111110",10000);
        // map.put("1-1-1-1-10",10000);
        // //碰壁眠4连
        // map.put("11110", 10000);
        // map.put("-1-1-1-10", 15000);
    }

    static String code;
    static Integer weight;

    /**
     * 计算每个位置的权重
     */
    public static void chessWeight() {
        // 遍历棋子位置数组，为每个位置计算权重
        for (int r = 0; r < chessPositionArray.length; r++) {
            for (int c = 0; c < chessPositionArray[r].length; c++) {
                if (chessPositionArray[r][c] == 0) {// 如果该位置没有棋子则开始统计
                    // 调用水平向左统计的方法
                    code = countHL(r, c);
                    // 根据棋子相连情况获取对应的权值
                    weight = map.get(code);
                    // 判断是否有该种棋子相连的情况
                    if (weight != null) {
                        // 累加权值
                        weightArray[r][c] += weight;
                    }
                    // 把另外七个方向统计完毕后，完成权值统计
                    code = countHR(r, c);
                    // 根据棋子相连情况获取对应的权值
                    weight = map.get(code);
                    if (weight != null) {
                        // 累加权值
                        weightArray[r][c] += weight;
                    }

                    code = countDown(r, c);
                    weight = map.get(code);
                    if (weight != null) {
                        weightArray[r][c] += weight;
                    }

                    code = countUp(r, c);
                    weight = map.get(code);
                    if (weight != null) {
                        weightArray[r][c] += weight;
                    }

                    code = countHLDown(r, c);
                    weight = map.get(code);
                    if (weight != null) {
                        // 累加权值
                        weightArray[r][c] += weight;
                    }

                    code = countHRDown(r, c);
                    weight = map.get(code);
                    if (weight != null) {
                        weightArray[r][c] += weight;
                    }

                    code = countHLUp(r, c);
                    weight = map.get(code);
                    if (weight != null) {
                        weightArray[r][c] += weight;
                    }

                    code = countHRUp(r, c);
                    weight = map.get(code);
                    if (weight != null) {
                        weightArray[r][c] += weight;
                    }

                    // 判断两个2连在一条直线但中间有一个空位的情况
                    if ((countHL(r, c) + countHR(r, c) == "01100110")
                            || (countHL(r, c) + countHR(r, c) == "0-1-100-1-10")
                            || (countHL(r, c) + countHR(r, c) == "-11100110")
                            || (countHL(r, c) + countHR(r, c) == "1-1-100-1-10")
                            || (countHL(r, c) + countHR(r, c) == "0110-1110")
                            || (countHL(r, c) + countHR(r, c) == "0-1-101-1-10")
                            || (countHL(r, c) + countHR(r, c) == "-1110-1110")
                            || (countHL(r, c) + countHR(r, c) == "1-1-101-1-10")) {
                        weightArray[r][c] = weightArray[r][c] + 5000;
                    }

                    if ((countDown(r, c) + countUp(r, c) == "01100110")
                            || (countDown(r, c) + countUp(r, c) == "0-1-100-1-10")
                            || (countDown(r, c) + countUp(r, c) == "-11100110")
                            || (countDown(r, c) + countUp(r, c) == "1-1-100-1-10")
                            || (countDown(r, c) + countUp(r, c) == "0110-1110")
                            || (countDown(r, c) + countUp(r, c) == "0-1-101-1-10")
                            || (countDown(r, c) + countUp(r, c) == "-1110-1110")
                            || (countDown(r, c) + countUp(r, c) == "1-1-101-1-10")) {
                        weightArray[r][c] = weightArray[r][c] + 5000;
                    }

                    if ((countHLDown(r, c) + countHRUp(r, c) == "01100110")
                            || (countHLDown(r, c) + countHRUp(r, c) == "0-1-100-1-10")
                            || (countHLDown(r, c) + countHRUp(r, c) == "-11100110")
                            || (countHLDown(r, c) + countHRUp(r, c) == "1-1-100-1-10")
                            || (countHLDown(r, c) + countHRUp(r, c) == "0110-1110")
                            || (countHLDown(r, c) + countHRUp(r, c) == "0-1-101-1-10")
                            || (countHLDown(r, c) + countHRUp(r, c) == "-1110-1110")
                            || (countHLDown(r, c) + countHRUp(r, c) == "1-1-101-1-10")) {
                        weightArray[r][c] = weightArray[r][c] + 5000;
                    }

                    if ((countHRDown(r, c) + countHLUp(r, c) == "01100110")
                            || (countHRDown(r, c) + countHLUp(r, c) == "0-1-100-1-10")
                            || (countHRDown(r, c) + countHLUp(r, c) == "-11100110")
                            || (countHRDown(r, c) + countHLUp(r, c) == "1-1-100-1-10")
                            || (countHRDown(r, c) + countHLUp(r, c) == "0110-1110")
                            || (countHRDown(r, c) + countHLUp(r, c) == "0-1-101-1-10")
                            || (countHRDown(r, c) + countHLUp(r, c) == "-1110-1110")
                            || (countHRDown(r, c) + countHLUp(r, c) == "1-1-101-1-10")) {
                        weightArray[r][c] = weightArray[r][c] + 5000;
                    }

                    // 判断一个活2连和一个活1连在一条直线上但中间有一个空位的情况
                    if ((countHL(r, c) + countHR(r, c) == "0100110") || (countHL(r, c) + countHR(r, c) == "0-100-1-10")
                            || (countHL(r, c) + countHR(r, c) == "0110010")
                            || (countHL(r, c) + countHR(r, c) == "0-1-100-10")) {
                        weightArray[r][c] = weightArray[r][c] + 3000;
                    }

                    if ((countDown(r, c) + countUp(r, c) == "0100110") || (countDown(r, c) + countUp(r, c) == "0-100-1-10")
                            || (countDown(r, c) + countUp(r, c) == "0110010")
                            || (countDown(r, c) + countUp(r, c) == "0-1-100-10")) {
                        weightArray[r][c] = weightArray[r][c] + 3000;
                    }

                    if ((countHLDown(r, c) + countHRUp(r, c) == "0100110")
                            || (countHLDown(r, c) + countHRUp(r, c) == "0-100-1-10")
                            || (countHLDown(r, c) + countHRUp(r, c) == "0110010")
                            || (countHLDown(r, c) + countHRUp(r, c) == "0-1-100-10")) {
                        weightArray[r][c] = weightArray[r][c] + 3000;
                    }

                    if ((countHRDown(r, c) + countHLUp(r, c) == "0100110")
                            || (countHRDown(r, c) + countHLUp(r, c) == "0-100-1-10")
                            || (countHRDown(r, c) + countHLUp(r, c) == "0110010")
                            || (countHRDown(r, c) + countHLUp(r, c) == "0-1-100-10")) {
                        weightArray[r][c] = weightArray[r][c] + 3000;
                    }

                    // 眠3连的一端被堵了
                    // "1-1-1-10"&"010" "0-1-1-10"&"010"
                    if ((countHL(r, c) + countHR(r, c) == "1-1-1-10010")
                            || (countHL(r, c) + countHR(r, c) == "0101-1-1-10")
                            || (countHL(r, c) + countHR(r, c) == "1-1-1-100")
                            || (countHL(r, c) + countHR(r, c) == "01-1-1-10")
                            || (countHL(r, c) + countHR(r, c) == "1-1-1-100110")
                            || (countHL(r, c) + countHR(r, c) == "01101-1-1-10")) {
                        weightArray[r][c] = 1;
                    }

                    if ((countDown(r, c) + countUp(r, c) == "1-1-1-10010")
                            || (countDown(r, c) + countUp(r, c) == "0101-1-1-10")
                            || (countDown(r, c) + countUp(r, c) == "1-1-1-100")
                            || (countDown(r, c) + countUp(r, c) == "01-1-1-10")
                            || (countDown(r, c) + countUp(r, c) == "1-1-1-100110")
                            || (countDown(r, c) + countUp(r, c) == "01101-1-1-10")) {
                        weightArray[r][c] = 1;
                        ;
                    }

                    if ((countHLDown(r, c) + countHRUp(r, c) == "1-1-1-10010")
                            || (countHLDown(r, c) + countHRUp(r, c) == "0101-1-1-10")
                            || (countHLDown(r, c) + countHRUp(r, c) == "1-1-1-100")
                            || (countHLDown(r, c) + countHRUp(r, c) == "01-1-1-10")
                            || (countHLDown(r, c) + countHRUp(r, c) == "1-1-1-100110")
                            || (countHLDown(r, c) + countHRUp(r, c) == "01101-1-1-10")) {
                        weightArray[r][c] = 1;
                    }

                    if ((countHRDown(r, c) + countHLUp(r, c) == "1-1-1-10010")
                            || (countHRDown(r, c) + countHLUp(r, c) == "0101-1-1-10")
                            || (countHRDown(r, c) + countHLUp(r, c) == "1-1-1-100")
                            || (countHRDown(r, c) + countHLUp(r, c) == "01-1-1-10")
                            || (countHRDown(r, c) + countHLUp(r, c) == "1-1-1-100110")
                            || (countHRDown(r, c) + countHLUp(r, c) == "01101-1-1-10")) {
                        weightArray[r][c] = 1;
                    }

                    // 3连和1连在一条线上差一个棋位
                    if ((countHL(r, c) + countHR(r, c) == "0-1-1-100-10")
                            || (countHL(r, c) + countHR(r, c) == "0-101-1-1-10")
                            || (countHL(r, c) + countHR(r, c) == "01110010")
                            || (countHL(r, c) + countHR(r, c) == "010-11110")
                            || (countHL(r, c) + countHR(r, c) == "0-100-1-1-10")
                            || (countHL(r, c) + countHR(r, c) == "1-1-1-100-10")
                            || (countHL(r, c) + countHR(r, c) == "01001110")
                            || (countHL(r, c) + countHR(r, c) == "-11110010")) {
                        weightArray[r][c] = weightArray[r][c] + 4000;
                    }

                    if ((countDown(r, c) + countUp(r, c) == "0-1-1-100-10")
                            || (countDown(r, c) + countUp(r, c) == "0-101-1-1-10")
                            || (countDown(r, c) + countUp(r, c) == "01110010")
                            || (countDown(r, c) + countUp(r, c) == "010-11110")
                            || (countDown(r, c) + countUp(r, c) == "0-100-1-1-10")
                            || (countDown(r, c) + countUp(r, c) == "1-1-1-100-10")
                            || (countDown(r, c) + countUp(r, c) == "01001110")
                            || (countDown(r, c) + countUp(r, c) == "-11110010")) {
                        weightArray[r][c] = weightArray[r][c] + 4000;
                    }

                    if ((countHLDown(r, c) + countHRUp(r, c) == "0-1-1-100-10")
                            || (countHLDown(r, c) + countHRUp(r, c) == "0-101-1-1-10")
                            || (countHLDown(r, c) + countHRUp(r, c) == "01110010")
                            || (countHLDown(r, c) + countHRUp(r, c) == "010-11110")
                            || (countHLDown(r, c) + countHRUp(r, c) == "0-100-1-1-10")
                            || (countHLDown(r, c) + countHRUp(r, c) == "1-1-1-100-10")
                            || (countHLDown(r, c) + countHRUp(r, c) == "01001110")
                            || (countHLDown(r, c) + countHRUp(r, c) == "-11110010")) {
                        weightArray[r][c] = weightArray[r][c] + 4000;
                    }

                    if ((countHRDown(r, c) + countHLUp(r, c) == "0-1-1-100-10")
                            || (countHRDown(r, c) + countHLUp(r, c) == "0-101-1-1-10")
                            || (countHRDown(r, c) + countHLUp(r, c) == "01110010")
                            || (countHRDown(r, c) + countHLUp(r, c) == "010-11110")
                            || (countHRDown(r, c) + countHLUp(r, c) == "0-100-1-1-10")
                            || (countHRDown(r, c) + countHLUp(r, c) == "1-1-1-100-10")
                            || (countHRDown(r, c) + countHLUp(r, c) == "01001110")
                            || (countHRDown(r, c) + countHLUp(r, c) == "-11110010")) {
                        weightArray[r][c] = weightArray[r][c] + 4000;
                    }
                }
            }
        }
    }

    /**
     * 水平向左统计的方法
     * @param r 棋子行数
     * @param c 棋子列数
     * @return
     */
    public static String countHL(int r, int c) {
        //采用StringBuilder，效率更高
        StringBuilder code = new StringBuilder("0");
        // 存储第一颗出现的棋子
        int chess = 0;
        // 循环遍历
        for (int r1 = r - 1; r1 >= 0; r1--) {
            // 空位，没有棋子  code = 0
            if (chessPositionArray[r1][c] == 0) {
                // 相邻
                if (r1 + 1 == r) {
                    break;
                } else {
                    // 拼接code，用于计算权重
                    code.insert(0, chessPositionArray[r1][c]);
                    break;
                }
            } else {// 表示该位置有棋子
                if (chess == 0) {// 第一次出现棋子
                    // 记录棋子
                    chess = chessPositionArray[r1][c];
                    code.insert(0, chessPositionArray[r1][c]);
                } else if (chess == chessPositionArray[r1][c]) {
                    code.insert(0, chessPositionArray[r1][c]);
                } else {
                    code.insert(0, chessPositionArray[r1][c]);
                    break;
                }
            }
        }
        return new String(code);
    }

    // 水平向右统计的方法
    public static String countHR(int r, int c) {
        StringBuilder code = new StringBuilder("0");
        int chess = 0;// 存储第一颗出现的棋子
        // 循环遍历
        for (int r1 = r + 1; r1 < coloum; r1++) {
            if (chessPositionArray[r1][c] == 0) {// 表示空位沒有棋子
                if (r1 - 1 == r) {// 相邻
                    break;
                } else {
                    code.insert(0, chessPositionArray[r1][c]);
                    break;
                }
            } else {// 表示该位置有棋子
                if (chess == 0) {// 第一次出现棋子
                    chess = chessPositionArray[r1][c];// 记录棋子
                    code.insert(0, chessPositionArray[r1][c]);
                } else if (chess == chessPositionArray[r1][c]) {
                    code.insert(0, chessPositionArray[r1][c]);
                } else {
                    code.insert(0, chessPositionArray[r1][c]);
                    break;
                }
            }
        }
        return new String(code);
    }

    // 竖直向下统计的方法
    public static String countDown(int r, int c) {
        StringBuilder code = new StringBuilder("0");
        int chess = 0;// 存储第一颗出现的棋子
        // 循环遍历
        for (int c1 = c - 1; c1 >= 0; c1--) {
            if (chessPositionArray[r][c1] == 0) {// 表示空位沒有棋子
                if (c1 + 1 == c) {// 相邻
                    break;
                } else {
                    code.insert(0, chessPositionArray[r][c1]);
                    break;
                }
            } else {// 表示该位置有棋子
                if (chess == 0) {// 第一次出现棋子
                    chess = chessPositionArray[r][c1];// 记录棋子
                    code.insert(0, chessPositionArray[r][c1]);
                } else if (chess == chessPositionArray[r][c1]) {
                    code.insert(0, chessPositionArray[r][c1]);
                } else {
                    code.insert(0, chessPositionArray[r][c1]);
                    break;
                }
            }
        }
        return new String(code);
    }

    // 竖直向上统计的方法
    public static String countUp(int r, int c) {
        StringBuilder code = new StringBuilder("0");
        int chess = 0;// 存储第一颗出现的棋子
        // 循环遍历
        for (int c1 = c + 1; c1 < row; c1++) {
            if (chessPositionArray[r][c1] == 0) {// 表示空位沒没有棋子
                if (c1 - 1 == c) {// 相邻
                    break;
                } else {
                    code.insert(0, chessPositionArray[r][c1]);
                    break;
                }
            } else {// 表示该位置有棋子
                if (chess == 0) {// 第一次出现棋子
                    chess = chessPositionArray[r][c1];// 记录棋子
                    code.insert(0, chessPositionArray[r][c1]);
                } else if (chess == chessPositionArray[r][c1]) {
                    code.insert(0, chessPositionArray[r][c1]);
                } else {
                    code.insert(0, chessPositionArray[r][c1]);
                    break;
                }
            }
        }
        return new String(code);
    }

    // 左斜向下统计的方法
    public static String countHLDown(int r, int c) {
        StringBuilder code = new StringBuilder("0");
        int chess = 0;// 存储第一颗出现的棋子
        // 循环遍历
        for (int r1 = r - 1, c1 = c - 1; r1 >= 0 && c1 >= 0; r1--, c1--) {
            if (chessPositionArray[r1][c1] == 0) {// 表示空位沒有棋子
                if (c1 + 1 == c && r1 + 1 == r) {// 相邻
                    break;
                } else {
                    code.insert(0, chessPositionArray[r1][c1]);
                    break;
                }
            } else {// 表示该位置有棋子
                if (chess == 0) {// 第一次出现棋子
                    chess = chessPositionArray[r1][c1];// 记录棋子
                    code.insert(0, chessPositionArray[r1][c1]);
                } else if (chess == chessPositionArray[r1][c1]) {
                    code.insert(0, chessPositionArray[r1][c1]);
                } else {
                    code.insert(0, chessPositionArray[r1][c1]);
                    break;
                }
            }
        }
        return new String(code);
    }

    // 右斜向下统计的方法
    public static String countHRDown(int r, int c) {
        StringBuilder code = new StringBuilder("0");
        int chess = 0;// 存储第一颗出现的棋子
        // 循环遍历
        for (int r1 = r + 1, c1 = c - 1; c1 >= 0 && r1 < coloum; r1++, c1--) {
            if (chessPositionArray[r1][c1] == 0) {// 表示空位沒有棋子
                if (r1 - 1 == r && c1 + 1 == c) {// 相邻
                    break;
                } else {
                    code.insert(0, chessPositionArray[r1][c1]);
                    break;
                }
            } else {// 表示该位置有棋子
                if (chess == 0) {// 第一次出现棋子
                    chess = chessPositionArray[r1][c1];// 记录棋子
                    code.insert(0, chessPositionArray[r1][c1]);
                } else if (chess == chessPositionArray[r1][c1]) {
                    code.insert(0, chessPositionArray[r1][c1]);
                } else {
                    code.insert(0, chessPositionArray[r1][c1]);
                    break;
                }
            }
        }
        return new String(code);
    }

    // 左斜向上统计的方法
    public static String countHLUp(int r, int c) {
        StringBuilder code = new StringBuilder("0");
        int chess = 0;// 存储第一颗出现的棋子
        // 循环遍历
        for (int r1 = r - 1, c1 = c + 1; c1 < row && r1 >= 0; c1++, r1--) {
            if (chessPositionArray[r1][c1] == 0) {// 表示空位沒有棋子
                if (r1 + 1 == r && c1 - 1 == c) {// 相邻
                    break;
                } else {
                    code.insert(0, chessPositionArray[r1][c1]);
                    break;
                }
            } else {// 表示该位置有棋子
                if (chess == 0) {// 第一次出现棋子
                    chess = chessPositionArray[r1][c1];// 记录棋子
                    code.insert(0, chessPositionArray[r1][c1]);
                } else if (chess == chessPositionArray[r1][c1]) {
                    code.insert(0, chessPositionArray[r1][c1]);
                } else {
                    code.insert(0, chessPositionArray[r1][c1]);
                    break;
                }
            }
        }
        return new String(code);
    }

    // 右斜向上统计的方法
    public static String countHRUp(int r, int c) {
        StringBuilder code = new StringBuilder("0");
        int chess = 0;// 存储第一颗出现的棋子
        // 循环遍历
        for (int r1 = r + 1, c1 = c + 1; r1 < coloum && c1 < row; r1++, c1++) {
            if (chessPositionArray[r1][c1] == 0) {// 表示空位沒有棋子
                if (r1 - 1 == r && c1 - 1 == c) {// 相邻
                    break;
                } else {
                    code.insert(0, chessPositionArray[r1][c1]);
                    break;
                }
            } else {// 表示该位置有棋子
                if (chess == 0) {// 第一次出现棋子
                    chess = chessPositionArray[r1][c1];// 记录棋子
                    code.insert(0, chessPositionArray[r1][c1]);
                } else if (chess == chessPositionArray[r1][c1]) {
                    code.insert(0, chessPositionArray[r1][c1]);
                } else {
                    code.insert(0, chessPositionArray[r1][c1]);
                    break;
                }
            }
        }
        return new String(code);
    }
}
