package com.gobang;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * 五子棋的主页面
 * JPanel :可以为添加到窗体中的轻型控件提供通用的容器,可以有效帮助样式改进
 */
public class GobangMain extends JPanel implements Gobang {

    // 用于存放棋子
    private ArrayList<Chess> chessArray = new ArrayList<>();

    // 动作监听器
    GobangListener gl = new GobangListener(this, chessArray);

    // 实例化JPanel面板对象，作为东边放置按钮的面板
    JPanel eastPanel = new JPanel();

    // 实例化单选按钮分组对象
    ButtonGroup bg = new ButtonGroup();

    public void initUI() {
        JFrame frame = new JFrame("五子棋");  // 界面左上角名字
        frame.setSize(780, 635);   // 界面大小
        /**
         * 简单来说frame.setDefaultCloseOperation()方法是为了指定窗口在用户发起“close”（×除窗口）后qo，其取值一共有四种：
         * 1.DO_NOTHING_ON_CLOSE，当你点击×除键时，它的反应就如其表达的意思不执行任何操作在close上，设置为该值是，点击×除键，界面无任何反应。
         * 2.HIDE_ON_CLOSE,当你点击×除键时,它会自动隐藏该窗口但是在任务管理器中仍旧可以发现窗口仍在运行。
         * 3.DISPOSE_ON_CLOSE，当你点击×除键时,它会仅仅关闭本窗口，而不会对应用程序下创建的其他窗口产生影响。
         * 4.EXIT_ON_CLOSE，它是使用 System exit 方法退出应用程序，当你点击关闭时，应用程序创建下的所有窗口都会被关闭。
         * 默认情况下，被设置为 HIDE_ON_CLOSE。
         */
        frame.setDefaultCloseOperation(3);  // 设置用户在此窗体上发起 “close” 时默认执行的操作
        // 设置窗口相对于指定组件的位置   为 null，此窗口将置于屏幕的中央
        frame.setLocationRelativeTo(null);
        // 设置窗口大小不可改变  默认为true
        frame.setResizable(false);
        // 设置页面布局
        frame.setLayout(new BorderLayout());

        // 设置棋盘面板的背景颜色
        this.setBackground(Color.LIGHT_GRAY);

        // 设置东边面板的布局方式为流式布局居中对齐
        eastPanel.setLayout(new FlowLayout());
        // 设置面板容器组件的宽度和高度
        eastPanel.setPreferredSize(new Dimension(150, 0));


        for (int i = 0; i < messageArray.length; i++) {
            if (i < 3) {
                JButton button = new JButton(messageArray[i]);
                // 设置按钮样式
                button.setPreferredSize(new Dimension(140, 80));
                // 添加按钮
                eastPanel.add(button);
                // 监听 开始新游戏 这个动作
                button.addActionListener(gl);
            }
            if (i == 3) {
                JLabel label = new JLabel(messageArray[i]);
                eastPanel.add(label);
            }
            if(i > 3) {
                JRadioButton button = new JRadioButton(messageArray[i]);
                // 取消默认选中
                button.setSelected(false);
                // 单选按钮
                bg.add(button);
                eastPanel.add(button);

                // 监听 人人或人机 这个动作
                button.addActionListener(gl);
            }
        }

        // 将棋盘面板添加到窗体的中间部分    this --> paint(Graphics g)
        frame.add(this, BorderLayout.CENTER);
        // 将eastPanel添加到窗体上的东边
        frame.add(eastPanel, BorderLayout.EAST);
        // 把图形界面设置为可见
        frame.setVisible(true);
    }

    /**
     * paint(Graphics g) 要在类继承JFrame或者JPanel两个属性下才能实现这个方法，并且这个方法是系统自动调用的
     *
     * 绘制棋盘，用重绘来画出棋盘，保证棋盘要一开始就出现在界面上，并且要一直存在，不能消失，
     * @param g
     */
    public void paint(Graphics g) {
        // paint(g)函数会重绘图像，要加上super.paint(g)，表示在原来图像的基础上，再画图。如果不加super.paint(g)，重绘时，会将原有的绘制清空，再根据paint(g)函数绘制
        super.paint(g);
        for (int i = 0; i < coloum; i++) {
            g.drawLine(X, Y + size * i, X + size * (coloum - 1), Y + size * i);// 横线 //格子40
            g.drawLine(X + size * i, Y, X + size * i, Y + size * (coloum - 1));// 竖线 //格子40
        }
        // 棋子
        for (int i = 0; i < chessArray.size(); i++) {
            Chess e = chessArray.get(i);
            g.setColor(e.color);
            /**
             * 使用当前颜色填充外接指定矩形框的椭圆。
             * 参数：
             *  x - 要填充椭圆的左上角的 x 坐标。
             *  y - 要填充椭圆的左上角的 y 坐标。
             *  width - 要填充椭圆的宽度。
             *  height - 要填充椭圆的高度。
             */
            g.fillOval(X + e.row * size - size / 2, Y + e.coloum * size - size / 2, size, size);

            // 标识最新下的棋子
            if(i == chessArray.size()-1){
                g.setColor(Color.red);
                g.drawRect(X + e.row * size - size/2 , Y + e.coloum * size - size/2, size, size);
            }
        }
    }

}
