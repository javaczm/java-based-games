package com.gobang;

/**
 * 用于判断输赢
 */
public class Judge implements Gobang {

    // 相连棋子的数量
    private int count;


    boolean judge(int x, int y){
        if(judgeWinner(x, y) >= 5){
            return true;
        }
        return  false;
    }

    /**
     * 判断输赢，当五颗棋子在横 纵 斜向连成直线，则为胜利
     * @param x 当前棋子的row
     * @param y 当前棋子的coloum
     * @return 连成直线的棋子个数
     */
    private int judgeWinner(int x, int y){
        //横向检查
        count = 1;
        // 从当前棋子往右数，直到棋盘边界
        for (int i = x + 1;i < row;i++){
            // 有棋子
            if(chessPositionArray[x][y] != 0){  // 这个判断其实有点多余，下了棋才调用这个方法，chessPositionArray[x][y]肯定不为0，否则count也不能为1
                if(chessPositionArray[x][y] == chessPositionArray[i][y]){
                    count++;
                }else {
                    break;
                }
            }
        }
        // 从当前棋子往左数，知道棋盘边界
        for (int i = x - 1;i >= 0;i--){
            // 有棋子
            if(chessPositionArray[x][y] != 0){  // 这个判断其实有点多余，下了棋才调用这个方法，chessPositionArray[x][y]肯定不为0，否则count也不能为1
                if(chessPositionArray[x][y] == chessPositionArray[i][y]){
                    count++;
                }else {
                    break;
                }
            }
        }
        if (count >= 5){
            return count;
        }

        //纵向检查
        count = 1;
        // 从当前棋子往上数，直到棋盘边界
        for (int i = y + 1;i < coloum;i++){
            // 有棋子
            if(chessPositionArray[x][y] != 0){  // 这个判断其实有点多余，下了棋才调用这个方法，chessPositionArray[x][y]肯定不为0，否则count也不能为1
                if(chessPositionArray[x][y] == chessPositionArray[x][i]){
                    count++;
                }else {
                    break;
                }
            }
        }
        // 从当前棋子往下数，直到棋盘边界
        for (int i = y - 1;i >= 0;i--){
            // 有棋子
            if(chessPositionArray[x][y] != 0){  // 这个判断其实有点多余，下了棋才调用这个方法，chessPositionArray[x][y]肯定不为0，否则count也不能为1
                if(chessPositionArray[x][y] == chessPositionArray[x][i]){
                    count++;
                }else {
                    break;
                }
            }
        }

        if (count >= 5){
            return count;
        }

        // 斜向检查  左上到右下
        count = 1;
        // 从当前棋子往左上数，直到棋盘边界
        for(int i = x - 1, j = y + 1; i >= 0 && j < coloum; i--, j++ ){
            // 有棋子
            if(chessPositionArray[x][y] != 0){  // 这个判断其实有点多余，下了棋才调用这个方法，chessPositionArray[x][y]肯定不为0，否则count也不能为1
                if(chessPositionArray[x][y] == chessPositionArray[i][j]){
                    count++;
                }else {
                    break;
                }
            }
        }
        // 从当前棋子往右下数，直到棋盘边界
        for(int i = x + 1, j = y - 1;i < row && j >= 0; i++, j--){
            // 有棋子
            if(chessPositionArray[x][y] != 0){  // 这个判断其实有点多余，下了棋才调用这个方法，chessPositionArray[x][y]肯定不为0，否则count也不能为1
                if(chessPositionArray[x][y] == chessPositionArray[i][j]){
                    count++;
                }else {
                    break;
                }
            }
        }

        if(count >= 5){
            return count;
        }

        // 斜向检查  右上到左下
        count = 1;
        // 从当前棋子往右上数，直到棋盘边界
        for(int i = x + 1, j = y + 1; i < row && j < coloum; i++, j++ ){
            // 有棋子
            if(chessPositionArray[x][y] != 0){  // 这个判断其实有点多余，下了棋才调用这个方法，chessPositionArray[x][y]肯定不为0，否则count也不能为1
                if(chessPositionArray[x][y] == chessPositionArray[i][j]){
                    count++;
                }else {
                    break;
                }
            }
        }
        // 从当前棋子往左下数，直到棋盘边界
        for(int i = x - 1, j = y - 1;i >= 0 && j >= 0; i--, j--){
            // 有棋子
            if(chessPositionArray[x][y] != 0){  // 这个判断其实有点多余，下了棋才调用这个方法，chessPositionArray[x][y]肯定不为0，否则count也不能为1
                if(chessPositionArray[x][y] == chessPositionArray[i][j]){
                    count++;
                }else {
                    break;
                }
            }
        }
        if(count >= 5){
            return count;
        }

        return count;
    }
}
