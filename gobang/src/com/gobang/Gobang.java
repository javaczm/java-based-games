package com.gobang;

/**
 * 经常使用到的公共参数
 */
public interface Gobang {
    public static final int size = 40; // 设置格子大小为40
    public static final int X = 20, Y = 20; // 设置棋盘右上角的位置
    public static final int row = 15;       //设置行数
    public static final int coloum = 15;    //设置列数
    public static final int[][] chessPositionArray = new int[row][coloum];   //记录棋子位置的数组
    public static final int[][] weightArray = new int[row][coloum];    //记录棋盘每个位置的权值
    public static final boolean flag[] = new boolean[2];    //记录选择的模式    flag[0]:人人对战  flag[1]:人机对战

    // 定义数组，存储组件上要显示的文字信息
    public static final String[] messageArray = { "开始新游戏", "悔棋", "认输", "对战模式：", "人人对战", "简单人机" ,"困难人机","地狱人机"};
}