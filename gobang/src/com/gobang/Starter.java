package com.gobang;

/**
 * 启动类
 */
public class Starter {
    public static void main(String[] args) {
        GobangMain gm = new GobangMain();
        gm.initUI();
    }
}
