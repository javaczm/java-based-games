package com.gobang;

import java.awt.*;

/**
 * 棋子
 */
public class Chess{
    int row;  //棋子位置  对应的是哪条横线（0-14）
    int coloum;  //棋子位置  对应的是哪条竖线（0-14）
    Color color;   // 棋子的颜色

    public Chess(int row, int coloum, Color color) {
        this.row = row;
        this.coloum = coloum;
        this.color = color;
    }

}
