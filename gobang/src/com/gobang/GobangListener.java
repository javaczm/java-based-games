package com.gobang;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * 要根据鼠标点击的位置来下棋和选择的模式进行人人对战和人机对战，
 * 所以这里我们要用到Java里面自带鼠标监(MouseAdapter)和动作监听(ActionListener),
 * 要把棋子画到棋盘上，还要用到画笔(Graphics)
 */
public class GobangListener extends MouseAdapter implements ActionListener, Gobang {

    private GobangMain gm;   // 棋盘面板对象
    private Graphics g;   // 画笔对象
    private boolean cco = true;   // 记录下黑棋还是白棋   true:黑棋  false:白棋
    private boolean fff = true;   // 记录是否能悔棋
    private boolean ggg = true;   // 记录是否能认输
    private ArrayList<Chess> chessArray;  //用于存放棋子
    // 真人棋子位置
    int playerRow, playerColoum;
    // 人机棋子位置
    int machineRow, machineColoum;
    // 棋子位置权重值
    int max;
    // 难度系数
    String difficultyGrade = "";

    public GobangListener(GobangMain gm, ArrayList<Chess> chessArray) {
        this.gm = gm;
        this.chessArray = chessArray;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // 开始新游戏
        if(e.getActionCommand().equals(messageArray[0])){
            // 将棋盘数组元素初始化为0
            for(int i = 0;i<chessPositionArray.length;i++){
                for(int j = 0;j<chessPositionArray[i].length;j++){
                    chessPositionArray[i][j] = 0;
                }
            }
            cco = true;  // 记录下黑棋
            fff = true;  // 记录能悔棋
            ggg = true;  // 记录能认输
            // 鼠标监听器    this是实现当前类中的监听方法
            gm.addMouseListener(this);
            // 清空全部棋子
            chessArray.clear();
            // 是重绘component的方法     repaint()这个方法是一个具有刷新页面效果的方法，如果你要页面进行重画就可以调用
            gm.repaint();
        }

        // 悔棋
        if(e.getActionCommand().equals(messageArray[1])){
            // 人人对战悔棋
            if(flag[0]){
                // 允许悔棋
                if(fff){
                    //当有棋子
                    if(chessArray.size() > 1){
                        chessPositionArray[playerRow][playerColoum] = 0;
                        // 获取悔棋前一个棋子的位置
                        Chess aaa = chessArray.get(chessArray.size()-2);
                        playerRow = aaa.row;
                        playerRow = aaa.coloum;
                        chessArray.remove(chessArray.size()-1);
                        // 将下棋权交还给悔棋方
                        cco = !cco;
                        gm.repaint();
                    }
                }
            }
            // 人机对战悔棋
            if(flag[1]){
                // 允许悔棋
                if(fff){
                    // 只有在真人棋权时才允许悔棋
                    if(cco){
                        //当有棋子
                        if(chessArray.size() > 2){
                            chessPositionArray[machineRow][machineColoum] = 0;
                            // 获取悔棋前一个棋子的位置
                            Chess aaa = chessArray.get(chessArray.size()-2);
                            playerRow = aaa.row;
                            playerColoum = aaa.coloum;
                            chessArray.remove(chessArray.size()-1);
                            chessPositionArray[playerRow][playerColoum] = 0;

                            Chess bbb = chessArray.get(chessArray.size() - 2);
                            machineRow = bbb.row;
                            machineColoum = bbb.coloum;
                            chessArray.remove(chessArray.size()-1);

                            gm.repaint();
                        }
                    }
                }
            }

        }

        // 认输
        if(e.getActionCommand().equals(messageArray[2])){
            // 允许认输
            if(ggg){
                if(cco){
                    JOptionPane.showMessageDialog(gm, "白棋获胜");
                }else {
                    JOptionPane.showMessageDialog(gm, "黑棋获胜");
                }
            }
            // 移除鼠标监听器
            gm.removeMouseListener(this);
            // 置为不可悔棋状态
            fff = false;
            // 置为不可认输状态
            ggg = false;
        }

        // 选择人人对战模式  flag[0]:true  , flag[1]:false
        if(e.getActionCommand().equals(messageArray[4])){
            flag[0] = true;
            flag[1] = false;
            // 将记录棋子位置的数组 置为0
            for(int i = 0;i<chessPositionArray.length;i++){
                Arrays.fill(chessPositionArray[i], 0);
            }
            cco=true;  //黑棋先手
            fff=true; //还没下就允许悔棋？？
            ggg=true;
            // 清空全部棋子
            chessArray.clear();
            // 是重绘component的方法     repaint()这个方法是一个具有刷新页面效果的方法，如果你要页面进行重画就可以调用
            gm.repaint();
        }

        // 选择人机对战模式(简单人机)  flag[0]:false  , flag[1]:true
        if(e.getActionCommand().equals(messageArray[5])){
            // 难度系数
            difficultyGrade = messageArray[5];
            flag[0] = false;
            flag[1] = true;
            // 将记录棋子位置的数组 置为0
            for(int i = 0;i<chessPositionArray.length;i++){
                Arrays.fill(chessPositionArray[i], 0);
            }
            cco=true;  //黑棋先手
            fff=true; //还没下就允许悔棋？？
            ggg=true;
            // 清空全部棋子
            chessArray.clear();
            // 是重绘component的方法     repaint()这个方法是一个具有刷新页面效果的方法，如果你要页面进行重画就可以调用
            gm.repaint();
        }

        // 选择人机对战模式(困难人机)  flag[0]:false  , flag[1]:true
        if(e.getActionCommand().equals(messageArray[6])){
            // 难度系数
            difficultyGrade = messageArray[6];
            flag[0] = false;
            flag[1] = true;
            // 将记录棋子位置的数组 置为0
            for(int i = 0;i<chessPositionArray.length;i++){
                Arrays.fill(chessPositionArray[i], 0);
            }
            cco=true;  //黑棋先手
            fff=true; //还没下就允许悔棋？？
            ggg=true;
            // 清空全部棋子
            chessArray.clear();
            // 是重绘component的方法     repaint()这个方法是一个具有刷新页面效果的方法，如果你要页面进行重画就可以调用
            gm.repaint();
        }

        // 选择人机对战模式(困难人机)  flag[0]:false  , flag[1]:true
        if(e.getActionCommand().equals(messageArray[7])){
            // TODO
            JOptionPane.showMessageDialog(null, "敬请期待", "提示", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    /**
     * 鼠标松开的时候进行的操作
     * @param e  鼠标事件
     */
    public void mouseReleased(MouseEvent e){
        // 人人对战
        if(flag[0]){
            if(g == null){
                // 画笔对象
                g = gm.getGraphics();
            }
            // 当前棋子坐标
            int x = e.getX();
            int y = e.getY();
//            playerRow = (x - X + size/2)/size;
//            playerColoum = (y - Y + size/2)/size;
            // 获取棋子的在哪一条线上
            playerRow = Math.round(x / size);
            playerColoum = Math.round(y / size);
//            System.err.println(playerRow);
//            System.err.println(playerColoum);
            // 棋子在棋盘内
            if(playerRow < row && playerColoum < coloum){
                // 当前位置没有棋
                if(chessPositionArray[playerRow][playerColoum] == 0){
                    // 黑棋
                    if(cco){
                        // 将当前棋子位置的数组置为1
                        chessPositionArray[playerRow][playerColoum] = 1;
                        // 创建黑棋对象，将棋子信息存储
                        Chess sh = new Chess(playerRow, playerColoum, Color.BLACK);
                        // 将棋子加入数组队列
                        chessArray.add(sh);
                    }else {
                        // 白棋 为-1
                        chessPositionArray[playerRow][playerColoum] = -1;
                        // 创建白棋对象，将棋子信息存储
                        Chess sh = new Chess(playerRow, playerColoum, Color.WHITE);
                        // 将棋子加入数组队列
                        chessArray.add(sh);

                    }
                    // 判断比赛是否结束，没结束则交换棋子权
                    winOrNo(playerRow, playerColoum);
                }

            }
        }
        // 人机对战
        if(flag[1]){
            if(g == null){
                g = gm.getGraphics();
            }
            // 人下棋
            if(cco){
                int x = e.getX();
                int y = e.getY();
                playerRow = Math.round(x / size);
                playerColoum = Math.round(y / size);
                if(playerRow < row && playerColoum < coloum){
                    if(chessPositionArray[playerRow][playerColoum] == 0){
                        // 人下黑棋 黑棋为1
                        chessPositionArray[playerRow][playerColoum] = 1;
                        Chess sh = new Chess(playerRow,playerColoum,Color.BLACK);
                        chessArray.add(sh);

                        // 判断比赛是否结束，没结束则交换棋子权
                        winOrNo(playerRow, playerColoum);
                    }
                }
            }
            // 人机下棋
            if(!cco){
                AIX();
            }
        }
        // 调用棋盘
        gm.paint(g);
    }

    /**
     * 人机下棋
     */
    public void AIX(){
        // 将棋盘权重数组每个元素初始化为0
        for (int i = 0;i < weightArray.length;i++){
            for(int j = 0;j < weightArray[i].length;j++){
                weightArray[i][j] = 0;
            }
        }

        // 初始化权重值
        max = -1;

        // 根据选择的难度用不同的算法
        switch (difficultyGrade){
            case "简单人机":
                // 获取记录棋盘每个位置的权值的数组weightArray
                 EasyAI.chessWeight();
                 break;
            case "困难人机":
                // 获取记录棋盘每个位置的权值的数组weightArray
                DifficultAI.chessWeight();
                break;
            case "地狱人机":
                // TODO
                JOptionPane.showMessageDialog(null, "敬请期待", "提示", JOptionPane.INFORMATION_MESSAGE);
                break;
        }

        // 测试--输出权重数组
        for (int i = 0;i < weightArray.length;i++){
            for(int j = 0;j < weightArray[i].length;j++){
                System.out.print(weightArray[i][j] + " ");
            }
            System.out.println();
        }
        System.err.println(weightArray.length +"--++"+ weightArray[0].length);
        for(int i=0;i<weightArray.length;i++) {
            for(int j=0;j<weightArray[i].length;j++) {
                // 获取权重值最大的位置
                if(weightArray[i][j] > max && chessPositionArray[i][j] == 0){
                    max = weightArray[i][j];
                    machineRow = i;
                    machineColoum = j;
                }
            }
        }

        // 测试--输出行 列 权重
        System.out.println(machineRow +"--"+ machineColoum +"--"+ max);

        if(chessPositionArray[machineRow][machineColoum] == 0) {
            // 白棋为-1  黑棋为1
            chessPositionArray[machineRow][machineColoum] = -1;
            Chess sh=new Chess(machineRow,machineColoum,Color.WHITE);
            chessArray.add(sh);

            // 判断比赛是否结束，没结束则交换棋子权
            winOrNo(machineRow,machineColoum);
        }
    }

    /**
     * 判断比赛是否结束，没结束则交换棋子权
     * @param row
     * @param coloum
     */
    private void winOrNo(int row, int coloum){
        Judge jd = new Judge();
        // 每当下一次棋，判断是否胜利
        if(jd.judge(row, coloum)){
            if(cco){
                JOptionPane.showMessageDialog(gm, "黑棋获胜");
            }else {
                JOptionPane.showMessageDialog(gm, "白棋获胜");
            }
            // 移除鼠标监听器
            gm.removeMouseListener(this);
            // 置为不可悔棋状态
            fff = false;
            // 置为不可认输状态
            ggg = false;
        }
        // 没结束，则交换棋权
        cco = !cco;
    }
}
